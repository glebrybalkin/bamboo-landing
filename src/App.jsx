import './App.css'
import Navbar from "./components/navbar/Navbar";
import Footer from "./components/footer/Footer";
import {Routes, Route} from "react-router-dom";
import MainPage from "./pages/MainPage";
import PrivacyPage from "./pages/PrivacyPage";
import gsap from "gsap";
import { ScrollTrigger } from "gsap/ScrollTrigger";

function App() {
    gsap.registerPlugin(ScrollTrigger);

  return (
    <>
      <Navbar/>
        <Routes>
            <Route path="/" element={<MainPage/>}/>
            <Route path="/privacy" element={<PrivacyPage/>}/>
        </Routes>
      <Footer/>
    </>
  )
}

export default App
