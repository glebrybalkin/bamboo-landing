import {useLayoutEffect, useRef} from "react";
import gsap from "gsap";

const Logo = ({className, src}) => {
    const img = useRef();

    useLayoutEffect(()=> {
        gsap.to(img.current, {
            scrollTrigger: {
                trigger: img.current
            },
            opacity: 1,
            duration: 1.5,
            delay: 0.1

        })
    }, [])

    return (
        <img ref={img} className={className} src={src} alt=""/>
    )
}

export default Logo;