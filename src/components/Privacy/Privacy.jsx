import './Privacy.css'

const Privacy = () => {
    return (
        <section className="privacy">
            <div className="privacy__container container">
                <div className="privacy__item">
                    <h5 className="privacy__title">CybrlogiQ Digital PTY LTD Privacy Poplicy</h5>
                    <p className="privacy__text">We are bound by the Privacy Act 1988 (Cth) (Privacy Act) and the
                        Australian Privacy Principles (APPs). This privacy policy (Policy) explains how and why we
                        collect, use, hold and disclose your Personal Information.</p>
                    <p className="privacy__text">By using our services and products, you agree to us collecting,
                        holding, using, and disclosing your Personal Information in accordance with this Policy. If you
                        have any questions about our Policy or your Personal Information, please contact us at
                        info@cybrlogiq.com.</p>
                    <p className="privacy__text">Last Updated: 26 May 2023</p>

                </div>

                <div className="privacy__item">
                    <h5 className="privacy__title">What is Personal Information?</h5>
                    <p className="privacy__text">Personal information is any information or an opinion about an
                        identified individual or an individual who can be reasonably identified from the information or
                        opinion (Personal Information). Information or an opinion may be Personal Information regardless
                        of whether it is true.</p>
                </div>

                <div className="privacy__item">
                    <h5 className="privacy__title">What Personal Information do we collect and hold?</h5>
                    <p className="privacy__list-label">We collect different types of Personal Information for various
                        purposes to provide and improve
                        our products and services. The types of Personal Information we provide include:</p>
                    <ol className='privacy__list'>
                        <li>Personal Data - We collect Personal Information that you voluntarily provide us, such as
                            your name, email address, phone number, and other contact information;
                        </li>
                        <li>
                            <p className="privacy__list-label">Usage Data - We may collect information about how you
                                access, use and interact
                                with the website. [We do this by using a range of tools such as Google Analytics and
                                AdWords and Salesforce (Sales Cloud and Marketing Cloud). This information may
                                include:</p>
                            <ol className="privacy__list_lt" type="a">
                                <li>
                                    the location from which you have come to the site and the pages you have visited as
                                    well as collateral downloaded;
                                </li>
                                <li>
                                    technical data, which may include IP address, the types of devices you are using to
                                    access the website, device attributes, browser type, language and operating system.
                                </li>
                            </ol>
                        </li>
                        <li>Tracking and Cookies Data - We use cookies on the website. A cookie is a small text file
                            that the website may place on your device to store information. We may use persistent
                            cookies (which remain on your computer even after you close your browser) to store
                            information that may speed up your use of our website for any of your future visits to the
                            website. We may also use session cookies (which no longer remain after you end your browsing
                            session) to help manage the display and presentation of information on the website. You may
                            refuse to use cookies by selecting the appropriate settings on your browser. However, please
                            note that if you do this, you may not be able to use the full functionality of the website.
                        </li>
                    </ol>
                </div>

                <div className="privacy__item">
                    <h5 className="privacy__title">Why do we collect, hold and use your Personal Information?</h5>

                    <p className="privacy__list-label">We collect, hold and use your Personal Information so that we
                        can:</p>
                    <ol className='privacy__list'>
                        <li>
                            provide you with products and services, and manage our relationship with you;
                        </li>
                        <li>
                            contact you, for example, to respond to your queries or complaints, or if we need to tell
                            you something important;
                        </li>
                        <li>
                            comply with our legal obligations and assist government and law enforcement agencies or
                            regulators;
                        </li>
                        <li>
                            identify and tell you about other products or services that we think may be of interest to
                            you.
                        </li>
                    </ol>

                    <p className="privacy__text">If you do not provide us with your Personal Information, we may not be
                        able to provide you with our services, communicate with you or respond to your enquiries</p>
                </div>

                <div className="privacy__item">
                    <h5 className="privacy__title">How do we collect your Personal Information?</h5>
                    <p className="privacy__text">We store most information about you in computer systems and databases operated by either us or our external service providers.</p>
                    <p className="privacy__text">We implement and maintain processes and security measures to protect Personal Information which we hold from misuse, interference or loss, and from unauthorised access, modification or disclosure.</p>
                    <p className="privacy__list-label">These processes and systems include:</p>
                    <ol className='privacy__list'>
                        <li>
                            the use of identity and access management technologies to control access to systems on which information is processed and stored;
                        </li>
                        <li>
                            requiring all employees to comply with internal information security policies and keep information secure;
                        </li>
                        <li>
                            monitoring and regularly reviewing our practice against our own policies and against industry best practice.
                        </li>
                    </ol>
                    <p className="privacy__text">We will also take reasonable steps to destroy or de-identify Personal Information once we no longer require it for the purposes for which it was collected or for any secondary purpose permitted under the APPs.</p>

                </div>


                <div className="privacy__item">
                    <h5 className="privacy__title">Who do we disclose your Personal Information to, and why?</h5>

                    <p className="privacy__text">We may transfer or disclose your Personal Information to our related companies.</p>
                    <p className="privacy__text">We may disclose your Personal Information to third parties in accordance with this Policy in circumstances where you would reasonably expect us to disclose your information. </p>

                    <p className="privacy__list-label">For example, we may disclose your Personal Information to:</p>
                    <ol className='privacy__list'>
                        <li>
                            our third party service providers (for example, our IT providers) for the purposes of enabling them to provide their services for us or on our behalf;
                        </li>
                        <li>
                            third party agents or sub-contractors who assist us in providing information, products, services or direct marketing to you;
                        </li>
                        <li>
                            suppliers or other third parties with whom we have commercial relationships, for business, marketing, and related purposes;
                        </li>
                        <li>
                            third parties to collect and process data, such as Google Analytics and AdWords, Salesforce (Sales Cloud and Marketing Cloud);
                        </li>
                        <li>
                            our marketing providers;
                        </li>
                        <li>
                            our professional services advisors.
                        </li>
                    </ol>

                    <p className="privacy__text">We may disclose your Personal Information to recipients which are located outside of Australia for some of these purposes, including in the Unites States of America & Japan. </p>

                    <p className="privacy__text">Where we disclose Personal Information to third parties overseas, we will take reasonable steps to ensure that data security and appropriate privacy practices are maintained.</p>

                    <p className="privacy__list-label">We may also disclose your Personal Information to others where:</p>
                    <ol className='privacy__list'>
                        <li>
                            we are required or authorised by law to do so;
                        </li>
                        <li>
                            you may have expressly consented to the disclosure or the consent may be reasonably inferred from the circumstances;
                        </li>
                        <li>
                            we are otherwise permitted to disclose the information under the Privacy Act;
                        </li>
                        <li>
                            If the ownership or control of all or part of our business changes, we may transfer your Personal Information to the new owner.
                        </li>
                    </ol>

                </div>


                <div className="privacy__item">
                    <h5 className="privacy__title">Do we use your Personal Information for marketing?</h5>

                    <p className="privacy__text">We will use your Personal Information to offer you products and services we believe may interest you, but we will not do so if you tell us not to. These products and services may be offered by us, our related companies, our other business partners or our service providers.</p>

                    <p className="privacy__text">Where you receive electronic marketing communications from us, you may opt out of receiving further marketing communications by following the opt-out instructions provided in the communication.</p>

                </div>

                <div className="privacy__item">
                    <h5 className="privacy__title">Access to and correction of your Personal Information</h5>


                    <p className="privacy__text">You may access or request correction of the Personal Information that we hold about you by contacting us. Our contact details are set out below. There are some circumstances in which we are not required to give you access to your Personal Information.</p>

                    <p className="privacy__text">There is no charge for requesting access to your Personal Information, but we may require you to meet our reasonable costs in providing you with access (such as photocopying costs or costs for time spent on collating large amounts of material).</p>

                    <p className="privacy__text">We will respond to your requests to access or correct Personal Information in a reasonable time and will take all reasonable steps to ensure that the Personal Information we hold about you remains accurate and up to date.</p>

                </div>

                <div className="privacy__item">
                    <h5 className="privacy__title">Your rights under the EU GDPR</h5>

                    <p className="privacy__list-label">Under the European Union (EU) General Data Protection Regulation (GDPR), as a data subject you have the right to:</p>
                    <ol className='privacy__list'>
                        <li>
                            access your data;
                        </li>
                        <li>
                            have your data deleted or corrected where it is inaccurate;
                        </li>
                        <li>
                            object to your data being processed and to restrict processing;
                        </li>
                        <li>
                            withdraw consent to having your data processed;
                        </li>
                        <li>
                            have your data provided in a standard format so that it can be transferred elsewhere;
                        </li>
                        <li>
                            not be subject to a decision based solely on automated processing.
                        </li>
                    </ol>

                    <h6 className="privacy__subtitle">(Data Subject Rights)</h6>

                    <p className="privacy__text">We have processes in place to deal with Data Subject Rights requests. Our actions and responsibilities will depend on whether we are the controller or processer of the personal data at issue. Depending on our role as either a controller or processor, the process for enabling Data Subject Rights may differ, and are always subject to applicable law. Please refer to the Contact Details section of this Policy if you would like to make a Data Subject Rights request OR have a specific need for assistance with a Data Subject Rights request.</p>


                </div>

                <div className="privacy__item">
                    <h5 className="privacy__title">Links to third party sites</h5>


                    <p className="privacy__text">Our website may contain links to websites operated by third parties. If you access a third party website through our website, Personal Information may be collected by that third party website. Links to third party websites do not constitute endorsement, sponsorship, or approval of these websites.</p>

                    <p className="privacy__text">We make no representations or warranties in relation to the privacy practices of any third party website and we are not responsible for the privacy policies or the content of any third party website. Third party websites are responsible for informing you about their own privacy practices. We recommend that you examine each website’s privacy policy.</p>

                </div>

                <div className="privacy__item">
                    <h5 className="privacy__title">Complaints</h5>

                    <p className="privacy__text">If you have a complaint about the way in which we have handled any privacy issue, including your request for access or correction of your Personal Information, you should contact us. Our contact details are set out below.</p>

                    <p className="privacy__text">We will consider your complaint and determine whether it requires further investigation. We will notify you of the outcome of this investigation and any subsequent internal investigation.</p>

                    <p className="privacy__text">If you remain unsatisfied with the way in which we have handled a privacy issue, you may approach an independent advisor or contact the Office of the Australian Information Commissioner (OAIC) (www.oaic.gov.au) for guidance on alternative courses of action which may be available.</p>


                </div>

                <div className="privacy__item">
                    <h5 className="privacy__title">Contact Details</h5>


                    <p className="privacy__text">If you have any questions, comments, requests or concerns, please contact us at:</p>

                    <a className="privacy__email link-underline"
                       href="mailto:info@cybrlogiq.com">info@cybrlogiq.com</a>
                </div>

                <div className="privacy__item">
                    <h5 className="privacy__title">Changes to the Policy</h5>


                    <p className="privacy__text">From time to time, we may change our Policy on how we handle Personal Information or the types of Personal Information which we hold. Any changes to our Policy will be published on our website.</p>

                    <p className="privacy__text">You may obtain a copy of our current Policy from our website or by contacting us at the contact details above.</p>
                </div>
            </div>
        </section>
    );
};

export default Privacy;