import './About.css'

const About = () => {
    return (
        <section className="about">
            <a className="anchor" id="about"/>
            <img className="about__bg-image-left" src="/images/lines_1.svg" alt=""/>
            <img className="about__bg-image-right" src="/images/lines_2.svg" alt=""/>
            <div className="about__container container">
                <div className="about__label section-label">ABOUT</div>
                <h4 className="about__title">Who we Are</h4>
                <p className="about__text">CybrlogiQ is an outcomes based cyber security consultancy that is born in the
                    cloud. We unlock transformative value by securing your most valuable assets through solutions that
                    are risk-led, cloud native and fine-tuned by over 13 years of cloud experience in many
                    industries.</p>
            </div>
        </section>
    );
};

export default About;