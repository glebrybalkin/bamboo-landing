import './Clients.css'
import Logo from "../../UI/Logo.jsx";


const Clients = () => {
    return (
        <section className="clients">
            <div className="clients__container container">
                <div className="clients__label section-label">CLIENTS</div>
                <h4 className="clients__title">Our Customers</h4>
                <div className="clients__description">
                    Our team of certified experts are dedicated to providing personalized, cost-effective solutions that meet the unique needs of each of our clients.
                </div>
                <div className="clients__logo-container">
                    <Logo className="clients__logo" src="/images/clients_logo_1.svg"/>
                    <Logo className="clients__logo" src="/images/clients_logo_2.svg"/>
                    <Logo className="clients__logo" src="/images/clients_logo_3.svg"/>
                    <Logo className="clients__logo" src="/images/clients_logo_4.svg"/>
                    <Logo className="clients__logo" src="/images/clients_logo_5.svg"/>
                    <Logo className="clients__logo" src="/images/clients_logo_6.svg"/>
                </div>
            </div>
        </section>
    );
};

export default Clients;