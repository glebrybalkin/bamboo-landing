import './Contact.css'
import { useForm, ValidationError } from '@formspree/react';
import {useEffect, useRef} from "react";

const Contact = () => {

    const form = useRef()
    const [state, handleSubmit] = useForm("xlekyzbo");

    useEffect(() => {
        if (state.succeeded) {
            form.current.reset();
        }
    }, [state])

    return (
        <section className="contact">
            <a className="anchor" id="contact"/>
            <div className="contact__container container">
                <img className="contact__image contact__image_lg-md-devices" src="/images/contact.svg" alt=""/>
                    <div className="contact__info">
                        <div className="contact__label section-label">JOIN</div>
                        <h4 className="contact__title">Questions?</h4>
                        <h5 className="contact__subtitle">Just ask.</h5>
                        <img className="contact__image contact__image_sm-devices" src="/images/contact.svg" alt=""/>
                        <iframe name="preventToReloadPageIframe" style={{display: 'none'}}></iframe>
                        <form ref={form} onSubmit={handleSubmit} target="preventToReloadPageIframe" className='contact__form'>
                            <input required maxLength="50" className='contact__input' type="text" name="name" id='name'
                                   placeholder='Name'/>
                            <input required maxLength="50" className='contact__input' type="email" name="email" id='email'
                                   placeholder='Email'/>
                            <input maxLength="30" className='contact__input' type="tel" name="phone" id='phone'
                                   placeholder='Phone number'/>
                            <textarea required maxLength="300" className='contact__input contact__input_text' id='message' name="message" placeholder='Your message'/>
                            <button disabled={state.submitting} className={state.submitting ? 'contact__submit-button contact__submit-button_disabled' : 'contact__submit-button'} type="submit">Submit</button>
                        </form>
                        {state.succeeded &&
                            <div className='contact__submit-succeed'><img className='contact__submit-succeed-icon' src='/images/outline_check.svg' alt=''/>Your form has been submitted.</div>
                        }

                    </div>
            </div>
        </section>
    );
};

export default Contact;