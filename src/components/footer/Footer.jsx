import {Link} from "react-router-dom";
import './Footer.css'



const Footer = () => {
    return (
        <footer className="footer">
            <div className="footer__container container">
                <div className="footer__logo-email">
                    <Link to="/#hero"><img className="footer__logo" src="/images/logo_white.svg" alt=""/></Link>
                    <a className="footer__email link-underline" href="mailto:info@cybrlogiq.com">info@cybrlogiq.com</a>
                </div>
                <div className="footer__nav">
                    <ul className="footer__list">
                        <li className="footer__list-item"><Link className="footer__list-link link-underline"
                                                                to="/#services">Services</Link></li>
                        <li className="footer__list-item"><Link className="footer__list-link link-underline"
                                                                to="/#about">About</Link></li>
                        <li className="footer__list-item"><Link className="footer__list-link link-underline"
                                                                to="/#value">Value</Link></li>
                        <li className="footer__list-item"><Link className="footer__list-link link-underline"
                                                                to="/#contact">Join</Link></li>
                        <li className="footer__list-item"><Link className="footer__list-link link-underline" to="/privacy">Privacy
                            Policy</Link></li>
                    </ul>
                    <div>
                        <h5 className="footer__title">Location</h5>
                        <p className="footer__address">Burj Alfardan<br/>
                            16th Floor<br/>
                            Zone 98<br/>
                            Lusail District<br/>
                            Doha, Qatar</p>
                    </div>
                </div>
                <div>
                    <h5 className="footer__title">Follow Us</h5>

                    <div className="footer__social-list">
                        <a className="footer__social-link footer__social-link_first" href="">
                            <img className="footer__social-icon" src="/images/linkedIn.svg" alt=""/>
                        </a>
                        <a className="footer__social-link" href="">
                            <img className="footer__social-icon" src="/images/mediumFill.svg" alt=""/>
                        </a>
                        <a className="footer__social-link" href="">
                            <img className="footer__social-icon" src="/images/youtube.svg" alt=""/>
                        </a>
                    </div>
                </div>
            </div>
            <div className="footer__line"/>
            <div className="footer__copyright">Copyright ©&nbsp;<span
                className="footer__copyright_bold">CybrLogiQ</span>&nbsp;| Designed by&nbsp;<a
                href="https://bambooapps.eu/" target="_blank" className="footer__copyright_bold">BambooApps</a></div>
        </footer>
    );
};

export default Footer;