import './GoogleMap.css'


const GoogleMap = () => {
    return (
        <iframe className='google-map'
                src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3604.5197504114426!2d51.51666248649025!3d25.38740716619958!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x3e45c2d13ddd6169%3A0x4bb4bc5a42d3f76e!2sBurj%20Alfardan!5e0!3m2!1sen!2sby!4v1686146931046!5m2!1sen!2sby" allowFullScreen="" loading="lazy"
                referrerPolicy="no-referrer-when-downgrade"></iframe>
    );
};

export default GoogleMap;