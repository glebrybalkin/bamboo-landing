import './Hero.css'
const Hero = () => {
    return (
        <section className="hero">
            <a className="anchor" id="hero"/>
            <div className="hero__container container">
                <div className="hero__info">
                    <h2 className="hero__subtitle">Secure your cloud<br/>migration with</h2>
                    <h1 className="hero__title">CybrlogiQ</h1>
                </div>
                <img className="hero__image" src="/images/hero.svg" alt=""/>
            </div>
        </section>
);
};

export default Hero;