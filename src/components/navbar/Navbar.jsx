import {Link} from "react-router-dom";
import './Navbar.css'


const Navbar = () => {
    return (
        <header className="navbar">
            <div className="navbar__container container">
                <Link to="/#hero"><img className="navbar__logo" src="/images/logo_dark.svg" alt=""/>
                </Link>
                <nav className="navbar__nav">
                    <Link className="navbar__link link-underline" to="/#services">Services</Link>
                    <Link className="navbar__link link-underline" to="/#about">About</Link>
                    <Link className="navbar__link link-underline" to="/#value">Value</Link>
                    <Link className="navbar__link link-underline" to="/#contact">Join</Link>
                </nav>
                <a className="navbar__contact" href="mailto:careers@cybrlogiq.com">Contact</a>
            </div>
        </header>
    );
};

export default Navbar;