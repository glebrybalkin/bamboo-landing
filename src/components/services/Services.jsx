import './Services.css'

const Services = () => {
    return (
        <section className="services">
            <a className="anchor" id="services"/>
            <div className="services__container container">
                <div className="services__label section-label">SERVICES</div>
                <h4 className="services__title">What makes CybrlogIQ different?</h4>
                <div className="services__info">
                    <div className="item-services">
                        <img className="item-services__image" src="/images/services_1.svg" alt=""/>
                        <h5 className="item-services__title">Cloud Security Assessment<br/>and Strategy</h5>
                        <p className="item-services__text">Our team will conduct a comprehensive assessment of your
                            current cloud security posture to identify threats and recommend solutions to address any
                            issues. </p>
                    </div>
                    <div className="item-services">
                        <img className="item-services__image" src="/images/services_2.svg" alt=""/>
                        <h5 className="item-services__title">Cloud Security Architecture<br/>and Engineering</h5>
                        <p className="item-services__text">We design and implement custom cloud security architectures
                            to ensure the highest level of protection for your critical assets and data.</p>
                    </div>
                    <div className="item-services">
                        <img className="item-services__image" src="/images/services_3.svg" alt=""/>
                        <h5 className="item-services__title">Cloud Security Training</h5>
                        <p className="item-services__text">We deliver tailored cloud security training and awareness
                            programs designed to educate our client's workforce on the best practices, policies, and
                            procedures to maintain a secure cloud environment.</p>
                    </div>
                    <div className="item-services">
                        <img className="item-services__image" src="/images/services_4.svg" alt=""/>
                        <h5 className="item-services__title">Cloud Security Operations</h5>
                        <p className="item-services__text">We provide ongoing monitoring and management of your cloud
                            security infrastructure to ensure that your business is always protected.</p>
                    </div>
                </div>
            </div>
        </section>
    );
};

export default Services;