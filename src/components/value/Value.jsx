import './Value.css'

const Value = () => {
    return (
        <section className="value">
            <a className="anchor" id="value"/>
            <div className="value__container container">
                <div className="value__info">
                    <div className="value__label section-label">VALUE</div>
                    <h4 className="value__title">CybrlogiQ</h4>
                    <h5 className="value__subtitle">benefits to businesses</h5>
                    <ul className="value__list">
                        <li className="value__list-item">Reduced risk of data breaches and other security threats
                        </li>
                        <li className="value__list-item">Enable rapid consumption of cloud services while maintaining
                            security and reliability
                        </li>
                        <li className="value__list-item">Increased visibility into your cloud security posture</li>
                        <li className="value__list-item">Customized solutions that meet your specific needs and
                            budget
                        </li>
                        <li className="value__list-item">Enhanced compliance with industry regulations and
                            standards
                        </li>
                        <li className="value__list-item">Increased visibility into your cloud security posture</li>
                    </ul>
                </div>
                <img className="value__image" src="/images/value.svg" alt=""/>
            </div>
        </section>
    );
};

export default Value;