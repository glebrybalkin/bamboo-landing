import Hero from "../components/hero/Hero";
import Services from "../components/services/Services";
import About from "../components/about/About";
import Value from "../components/value/Value";
import Contact from "../components/contact/Contact";
import {useEffect} from "react";
import {useLocation} from "react-router-dom";
import Clients from "../components/clients/Clients";
import GoogleMap from "../components/googleMap/GoogleMap.jsx";
const MainPage = () => {

    let location = useLocation();

    useEffect(() => {
        let hash = location.hash;
        const removeHashCharacter = (str) => {
            return str.slice(1);
        };

        if (hash) {
            document.getElementById(removeHashCharacter(hash)).scrollIntoView({
                behavior: "smooth",
                inline: "nearest",
            });
        }
    }, [location]);

    return (
        <>
            <Hero/>
            <Services/>
            <About/>
            <Value/>
            <Clients/>
            <Contact/>
            <GoogleMap/>
        </>
    );
};

export default MainPage;