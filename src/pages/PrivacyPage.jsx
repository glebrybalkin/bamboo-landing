import Privacy from "../components/Privacy/Privacy";
import {useEffect} from "react";

const PrivacyPage = () => {


    useEffect(() => {
        document.documentElement.scrollTo({
            top:0,
            left: 0,
            behavior: "instant"
        })
    }, []);

    return (
        <Privacy/>
    );
};

export default PrivacyPage;